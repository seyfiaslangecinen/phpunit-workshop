<?php
require_once 'vendor/autoload.php';

/**
 * Load database schema
 */
$dotEnv = new \Dotenv\Dotenv('app/');
$dotEnv->load();

$dbConnection = new \PDO(getenv('DB_DSN'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'));
$dbConnection->exec("DROP TABLE IF EXISTS `rates`");
$dbConnection->exec("
CREATE TABLE `rates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `symbol` varchar(6) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

echo "Schemas created\n";
