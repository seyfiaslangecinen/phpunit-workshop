# PhpUnit Tutorial
This tutorial is a step by step hands-on TDD practise based on a simple Exchange Rate Rest API.

#### API Documentation
Open swagger.yml in [Swagger Editor][1] to check endpoints in detail

#### Project Setup
###### Vagrant Users
1. Clone Repository
`git clone git@bitbucket.org:seyfiaslangecinen/phpunit-workshop.git && cd phpunit-workshop`
2. Bootstrap virtual environment
`vagrant up`

###### Manual installation
1. Clone Repository
`git clone git@bitbucket.org:seyfiaslangecinen/phpunit-workshop.git && cd phpunit-workshop`
2. Install local PHP packages
`composer install`
3. Make a copy of env.example
`cp .env.example .env`
4. Change database credentials in .env file
5. Create database schemas by running
`php schema.php`

[1]: http://editor.swagger.io/#/